USE importRSS;
 
CREATE TABLE Usuario(
Id int IDENTITY(1,1)PRIMARY KEY NOT NULL,
Nombre Varchar(100) NOT NULL,
Apellidos varchar (100),
Alias nvarchar(10),
Email varchar(100),
Telefono varchar(20),
CodPostal varchar(20),
Ciudad varchar(100),
)
 
CREATE TABLE Categoria(
Id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
Nombre Varchar(100),
)
 
CREATE TABLE Post(
Id int IDENTITY(1,1) PRIMARY KEY NOT NULL,
IdUsuario int NOT NULL,
Fecha varchar(50) NOT NULL,
Titulo varchar (100) NOT NULL,
Contenido varchar (500) NOT NULL,
IdCategoria int NOT NULL,
constraint ID_PostUsuario foreign key (IdUsuario) references Usuario(Id),
constraint ID_CategoriaUsuario foreign key (IdCategoria) references Categoria(Id)
)

