package com.cesur;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.SAXException;
public class XmlParse{
    public List<String> leer(String ruta_xpath,String url_){
        List<String> list=new ArrayList<>();
        DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        try {
            URL url=new URL(url_);
            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            String entrada;
            String cadena="";
            while ((entrada = br.readLine()) != null){
                cadena = cadena + entrada;
            }
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource archivo = new InputSource();
            archivo.setCharacterStream(new StringReader(cadena)); 
            Document doc = db.parse(archivo);
         //   doc.getDocumentElement().normalize();
            

            // Create XPathFactory object
            XPathFactory xpathFactory = XPathFactory.newInstance();

            // Create XPath object
            XPath xpath = xpathFactory.newXPath();

            XPathExpression expr = xpath.compile(ruta_xpath);

            NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++)
                list.add(nodes.item(i).getNodeValue());
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException e ) {
            e.printStackTrace();
        }
        return list;
    }
}