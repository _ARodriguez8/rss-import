package com.cesur;
import java.util.List;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;
import java.util.Scanner;


/**
 * Hello world!
 *
 */
public class App{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="importRSS ";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    static Scanner sc=new Scanner (System.in);
    static bbdd bd=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
    /** 
     * @param args
     */
    public static void main(String[] args ){
        crearMenu();
    }
    private static void CrearBBDD(){
        String s=bd.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
        if(s.equals("0")){
            bd.modificarBBDD("CREATE DATABASE " + DBname);
            bd=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String query=LeeScript("scripts/BBDDBlog.sql");
            bd.modificarBBDD(query);
            bd.modificarBBDD("insert into clientes values ('Ale','ale@cesur.com','666666666','calle holaaa 13','41008','Sevilla')");
        }
    }
    private static String LeeScript(String fichero){
       String s="";
        try{
            String e;
            FileReader fr=new FileReader(fichero);
            BufferedReader br=new BufferedReader(fr);
            while((e=br.readLine())!=null) {
                s=s+e;
            }
            br.close();
        } catch(Exception e){
        }
       return s;
    }
    public static void crearMenu(){   
       boolean exit=false;
       int eleccion;
       try{
       while(!exit){ 
           System.out.println("0. Importar RSS ");
           System.out.println("1. Ver ultimos 10 post");
           System.out.println("2. Ver todos los post");
           System.out.println("3. Buscar por titulo o contenido una palabra (indicar la palabra por la que buscar)");
           System.out.println("4. Filtrar por categoria");
           System.out.println("5. Salir");
           System.out.println("Escribe una de las opciones");
           eleccion=sc.nextInt();
           switch(eleccion){
               case 0:
                   System.out.println("Opcion 0, introduce una URL");
                   importarRSS();
                   break;
               case 1:
                   System.out.println("Opcion 1");
                    ultimos10();
                   break;
                case 2:
                   System.out.println("Opcion 2");
                    mostrarPost();
                   break;
                case 3:
                   System.out.println("Opcion 3");
                   FiltroTituloPalabra();
                   break;
                case 4:
                   System.out.println("Opcion 4");
                   FiltroCategoria();
                   break;
                case 5:
                   exit=true;
                   break;
                default:
                   System.out.println("Introduzca un numero entre 0 y 5");
           } 
       }
    }catch(InputMismatchException e){
        System.out.println("Introduce un numero");
        sc.next();
    }
    }
public static void FiltroCategoria(){
    System.out.println("Introducir palabra por la que quiere realizar el filtro");
    String filtro=sc.nextLine();
    String consulta="select nombre from Categoria where Nombre like '%"+filtro+"%'"; 
    System.out.println(bd.leerBBDD(consulta,1));
    }
public static void FiltroTituloPalabra(){
    System.out.println("¿Por qué palabra quieres filtrar?");
    String filtro=sc.nextLine();
    String consulta="select Titulo, Contenido from Post where Titulo like '%"+filtro+"%' or Contenido like'%"+filtro+"%'";
    System.out.println(bd.leerBBDD(consulta,2));
    }
public static void mostrarPost(){
    String consultaPost="select post.Id, Titulo, Fecha, Usuario.Nombre, Categoria.Nombre from Post inner join Usuario on Post.IdUsuario=Usuario.Id inner join Categoria on Post.IdCategoria=Categoria.Id"; 
    System.out.println(bd.leerBBDD(consultaPost,5));
    }
public static void ultimos10(){
        String consulta="select top 10 Id, Titulo from Post order by Id"; 
        System.out.println(bd.leerBBDD(consulta, 2));
    }
public static void importarRSS(){
        // b.modificarBBDD("delete from clientes where id=" + ret);
        //b.leerBBDD("select top 1 * from clientes order by id desc",7);
        XmlParse x=new XmlParse();
        String url="";
        System.out.println("Introduce una url:");
        url=sc.nextLine();
        System.out.println(url);
        //url ="https://lenguajedemarcasybbdd.wordpress.com/feed/";
        List<String> titulos=x.leer("rss/channel/item/title/text()",url);
        List<String> creadores=x.leer("rss/channel/item/creator/text()",url);
        List<String> fechas=x.leer("rss/channel/item/pubDate/text()",url);
        List<String> categorias=x.leer("rss/channel/item/category/text()",url);
        List<String> descripciones=x.leer("rss/channel/item/description/text()",url);

        for(int i=0;i<titulos.size();i++){
            String consulta="Select count(1) from Usuario where Nombre='"+creadores.get(i)+"'";
            if(bd.leerBBDDUnDato(consulta).equals("0")){
                String insert="INSERT INTO Usuario(Nombre) VALUES ('"+creadores.get(i)+"')";
                bd.modificarBBDD(insert);
                System.out.println(creadores.get(i));
            }
            String consultaCat="Select count(1) from CATEGORIA where nombre='"+categorias.get(i)+"'";
            if(bd.leerBBDDUnDato(consultaCat).equals("0")){
                String insertCat="INSERT INTO Categoria (Nombre) VALUES ('"+categorias.get(i)+"')";
                bd.modificarBBDD(insertCat);
                System.out.println(categorias.get(i));
            }
            String selectUser="Select Id from Usuario where Nombre='"+creadores.get(i)+"'";
            String selectCat="Select Id from Categoria where Nombre='"+categorias.get(i)+"'";
            String insertTitulos="Insert INTO Post (IDUsuario, Titulo, Fecha, Contenido, IdCategoria) VALUES ("+bd.leerBBDDUnDato(selectUser)+",'"+titulos.get(i)+"','"+fechas.get(i)+"','"+descripciones.get(i)+"',"+bd.leerBBDDUnDato(selectCat)+")";
            System.out.println(insertTitulos);
            bd.modificarBBDD(insertTitulos);
            System.out.println(titulos.get(i));
            System.out.println(fechas.get(i));
            System.out.println(descripciones.get(i));
        }
    }
}